/***********************************************
 * Den otevřených dveří FIT ČVUT
 * autoři: Gabriela Hánová, Ing. Ivo Háleček
 * 2015
 **********************************************/

/**********************************************
 * NEMĚNIT - Výchozí parametry a definice
 *********************************************/

  #define BRIGHT 30       // doporucena hodnota jasu RGB led
  #define UP_TRESHOLD 700          // velikost souradnice joysticku v horni pozici  
  #define DOWN_TRESHOLD 300        // velikost souradnice joysticku v dolni pozici 
  #define RIGHT_TRESHOLD 700          // velikost souradnice joysticku v prave pozici  
  #define LEFT_TRESHOLD 300        // velikost souradnice joysticku v leve pozici 

  #define LEFT 1
  #define UP 2
  #define RIGHT 3
  #define DOWN 4
  #define CENTER 0
  
  int xPin = A1;          // nastaveni joysticku - x souradnice na pinu A1
  int yPin = A0;          // nastaveni joysticku - y souradnice na pinu A0
  
  int xPos = 0;           // velikost souradnice x na joysticku
  int yPos = 0;           // velikost souradnice y na joysticku 
  
  int redPin = 9;         // nastaveni pinu - cervena LED na pinu 9
  int greenPin = 10;      // nastaveni pinu - zelena LED na pinu 10
  int bluePin = 11;       // nastaveni pinu - modra LED na pinu 11

/*********************************************
 * NEMĚNIT - Pomocné předpřipravené funkce
 ********************************************/

  void setColor(int red, int green, int blue)  // funkce na nastaveni RGB led
  {
    analogWrite(redPin, red);
    analogWrite(greenPin, green);
    analogWrite(bluePin, blue);
  }

  void initJoystick(){ // nastavení pinů joysticku
    pinMode(xPin, INPUT);  // nastaveni pinu jako vstup
    pinMode(yPin, INPUT);  // nastaveni pinu jako vstup
  }

  int readJoystickKey(){
      xPos = analogRead(xPin);    // cteni hodnoty xx na joysticku
      delay(100);
      yPos = analogRead(yPin);     // cteni hodnoty y na joysticku
      delay(100);
    
      if (xPos > UP_TRESHOLD) { // nahoru
        return UP;
      }
    
      if (xPos < DOWN_TRESHOLD) { // dolu
        return DOWN;
      }
    
      if (yPos > RIGHT_TRESHOLD) { // doprava
        return RIGHT;
      }
    
      if (yPos < LEFT_TRESHOLD) { // doleva
        return LEFT;
      }
    
      return CENTER;
  }

/*********************************************
 * K vypracováni:
 ********************************************/

// provede se jednou po spuštění přípravku
//  - měla by obsahovat případné počáteční nastavení
void setup() 
{
 
}

// provádí se ve smyčce opakovaně
//  - měla by obsahovat popis chování
void loop()  
{

}

